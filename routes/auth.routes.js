const { Router } = require("express");
const { check } = require("express-validator");
const { login } = require("../controllers/auth.controller");
const { validate } = require("../middleware/validate");

const router = Router()

router.post('/login', [
    check('email','El email es obligatorio').notEmpty(),
    check('password','El password es obligatorio').notEmpty(),
    validate
], login)


module.exports = router