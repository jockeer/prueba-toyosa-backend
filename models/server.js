const express = require('express');
const cors = require('cors');
const { db } = require('../db/conexion');

class Server {
    constructor() {
        this.app = express();
        this.port = process.env.PORT || 8080

        this.database();
        
        this.middlewares();

        this.routes();
    }

    async database() {
        try {
            await db.connect();
            // await db.sync({ force: true }) // usar solo una ves para crear los modelos en la base de datos
            console.log('Database conectada')
        } catch (error) {
            console.log('No se pudo conectar a la bd')
            throw new Error(error);
        }
    }

    middlewares() {
        this.app.use(cors());
        this.app.use(express.json());
    }

    routes() {

        this.app.use('/api/auth', require('../routes/auth.routes'))
     
        
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log(`Servidor corriendo en el puerto ${this.port}`);
        })
    }
}

module.exports = Server;